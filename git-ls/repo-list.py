#!/usr/bin/env python3

import requests
from tabulate import tabulate
from configparser import ConfigParser

config = ConfigParser()
config.read('aws.conf')
private_token = config.get('global','private_token')
user_id = config.get('global','user_id')

#personal_token = 'x7HyUBs5jLJKoRzd4ugi'
#user_id = 'dword4'

base_url = 'https://gitlab.com/api/v4/'
repo_url = 'users/'+user_id+'/projects'

#url = "https://gitlab.com/api/v4/users/%s/projects?private_token=%s" % (user_id, private_token)
#print(url)

build_url = f"https://gitlab.com/api/v4/users/{user_id}/projects?private_token={private_token}"
full_url = build_url.replace('\'','')
res = requests.get(full_url).json()

table = []
for project in res:
    name = project['name']
    name_spaced = project['name_with_namespace']
    path = project['path']
    path_spaced = project['path_with_namespace']
    if project['description'] is None:
        description = ''
    else:
        description = project['description']
    #print(name,'|', description)
    table.append([name, description])

print(tabulate(table, headers=["name","description"]))
